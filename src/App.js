import Navbar from "./components/navbar/Navbar.js"
import PreFooter from './components/pre-footer/PreFooter.js'
import Footer from './components/footer/Footer.js'

import Home from "./pages/home/Home.js"
import Menu from "./pages/menu/Menu.js"
import PapaRewards from './pages/papa-rewards/PapaRewards.js'
import About from "./pages/about/About.js"
import Promotions from "./pages/promotions/Promotions.js"
import { Router } from "./router/Router.js"

export default function App()
{

    const 
    router = new Router(),
    app = document.getElementById('app'),
    main = document.createElement('main')
    
   
    main.id='main'
    app.innerHTML=''

    app.appendChild(Navbar())
    app.appendChild(main)



    app.appendChild(PreFooter())
    app.appendChild(Footer())

    router.route('#/',Home())
    router.route('#/menu',Menu())
    router.route('#/promotions',Promotions())
    router.route('#/papa-rewards',PapaRewards())
    router.route('#/about-us',About())
    router.dispatch()

}