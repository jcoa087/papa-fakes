

export class Router
{
    constructor()
    {
        this.routes=[]
        this.components = []
        this.currentRoute = null
        this.main = null
    }
    
    render(route){

        try{
            this.main.innerHTML=''

            if(typeof route.component ==='object') this.main.appendChild(route.component)
          
            if(typeof route.component ==='string') this.main.innerHTML=route.component
           
            
        }catch(error){
            throw new Error(`Error loading component belonging to path ${route.Component} : ${error}`)
        }  
    
    }

    route(path,component)
    {   
        this.routes.push({'path':path,'component':component})
  
    }

    dispatch(){
        let {hash} = location
        this.main=document.getElementById('main')

        for(let route of this.routes)
        {
            if(route.path.match('^#\/search\/:[A-zA-z]+$'))
            {
                this.render(route)
                break
            }
            else if(hash === '#/' || !hash) 
            {    
         
                this.render(route)
                break
            }
            else if(hash === route.path ) 
            {      
                
                this.render(route)
                break
            }
        }

    }

}




