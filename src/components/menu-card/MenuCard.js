

export function MenuCard(img,name,description,linkDesc){

    const $card = document.createElement('div')
    $card.classList.add('food-card')
    $card.classList.add('food-item')
    $card.innerHTML=`
    <img src=${img} alt='Hot pizza'  class='food-item'/>
    <div class='item-content'>
        <h2 class='food-item'>${name}</h2>
        <p class='food-item'>${description}</p>
        <hr class='food-item'/>
        <a href='#/menu' ><span>${linkDesc}</span><img class='food-item' src='src/assets/icons/chevron-right.svg' /></a>
    </div>
    `

    return $card
}