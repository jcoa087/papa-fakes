




export default function PreFooter()
{
    const 
    preFooter = document.createElement('section'),
    container = document.createElement('div'),
    style = document.getElementById('dinamic-style')

    style.innerHTML+=`
    .pre-footer
    {
        background-color:#F3F3F7;
        width:100%;
    }
    .pre-footer .container
    {
        max-width:1150px;
        width:100%;
        margin:auto;
        display:grid;
        gap:3.5rem;
        grid-template-columns:repeat(auto-fit,minmax(200px,1fr));
        padding:  5rem 1rem 2rem 1rem;
    }
    .pre-footer .container div p
    {
        color:#616161;
        font-size:1rem;
        margin-top:.5rem;
    }
    .pre-footer .container div p a
    {
        color:#008764;
    }
    

    `
    preFooter.classList.add('pre-footer')
    container.classList.add('container')
    
    container.innerHTML=`
    <div>
        <h4>Llámanos, visítanos, o haz tu pedido en línea</h4>
        <p>¿Quieres tener la experiencia Papa John's en un restaurante? Visítanos y haz tu pedido. ¿Quieres comer en casa? No hay problema. Llámanos a tu 
        <a href='#/' >restaurante más cercano</a>,  o pide en línea a domicilio o para llevar.</p>
    </div>
    <div>
        <h4>Solicita una entrega sin contacto</h4>
        <p>Te entregamos la pizza sin contacto. Tú pagas de manera anticipada en línea y nosotros dejamos tu pedido en la puerta y nos apartaremos para que puedas recibirlo.
        </p>
    </div>
    <div>
        <h4>Más de 5,395 puntos de venta en todo el mundo</h4>
        <p>¡Sí, estamos en todo el mundo! Nuestros clientes disfrutan de nuestra pizza americana en África, Europa, Asia y América del Norte y del Sur.</p>
    </div>
    `
    preFooter.appendChild(container)

    return preFooter

}