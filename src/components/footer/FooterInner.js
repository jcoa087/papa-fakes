



export default function FooterInner()
{
    const footerInner = document.createElement('div')

    footerInner.classList.add('footer-inner')

    footerInner.innerHTML=`
    <div class='container'>
            <div class='footer-navigation'>
                <div class='footer-list'>
                    <h4>Nuestra Empresa</h4>
                    <ul>
                        <li>Acerca de nosotros</li>
                        <li>Entrega sin contacto</li>
                        <li>Tener una franquicia</li>
                    </ul>
                </div>
                <div class='footer-list'>
                    <h4>Nuestra Comida</h4>
                    <ul>
                        <li>Ingredientes</li>
                        <li>Nutrición</li>
                    </ul>
                </div>
                <div class='footer-list'>
                    <h4>Ayuda</h4>
                    <ul>
                        <li>Contáctanos</li>
                        <li>FAQ</li>
                        <li>Registrarse para recibir ofertas</li>
                        <li>Localizador de restaurantes</li>
                        <li>Descargar la app</li>
                        <li>Papa Talk</li>
                        <li>Solicita una factura</li>
                    </ul>
                </div>
                <div class='footer-form'>
                    <h3><span>¿Quieres más ofertas de pizzas?</span></h3>
                    <h3><span>¡Claro que las quieres!</span></h3>
                    <div class='form-wrapper' >
                        <input type='email' placeholder='Introduzca su dirección de correo electrónico' />
                        <button>Regístrate</button>
                    </div>
                    <p><span>Al registrarme, acepto que me manden comunicaciones publicitarias de Papa Johns por correo electrónico o mensajes de texto SMS.</span></p>
                </div>
            </div>
        </div>
    `

    return footerInner

}