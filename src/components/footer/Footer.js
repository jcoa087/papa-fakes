


import FooterInner from "./FooterInner.js"

export default function Footer()
{
    const 
    footer = document.createElement('footer'),
    style = document.getElementById('dinamic-style')
    footer.classList.add('footer')

    footer.appendChild(FooterInner())

    style.innerHTML+=`
    .footer
    {   color:white;width:100%;
    }

    .footer-inner
    {
        padding:3rem 0; background-color:#007B53;
        width:100%; margin:auto;
    }

    .footer-navigation
    {        
        max-width:1150px; width:100%;
        margin:auto; display:grid;
        grid-template-columns:minmax(03%,1fr) minmax(20%,1fr) minmax(20%,1fr) minmax(35%,1fr);
    }
    .footer-navigation .footer-form .form-wrapper input
    {
        padding:1rem; width:100%;
        border-radius:6px; border:none;
        outline:none; font-size:.9rem;
    }
    .footer-navigation .footer-form .form-wrapper button
    {
        background-color:black; color:white;
        padding:.5rem 3rem; border-radius:6px;
        outline:none; border:none;
    }
    .footer-navigation .footer-form p
    {
        font-size:.7rem;
    }
    .footer-navigation .footer-list ul li
    {
        font-size:.9rem;
    }
    `
    return footer
}