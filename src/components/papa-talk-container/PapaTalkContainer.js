



export default function PapaTalkContainer(title,first_description,second_description,btn_description)
{


    const contactFreeDelivery = document.createElement('section'),style = document.getElementById('dinamic-style')
    contactFreeDelivery.classList.add('papa-talk-container')
    contactFreeDelivery.innerHTML=`
    <div class='aside-image' >
        <img src='src/assets/images/papa-talk-container/pj-promo-image-large-papa-talk-photo.webp' />
    </div>
    <div class='aside-content' >
        <h2>${title} </h2>
        <p>${first_description}</p>
        <p>${second_description}</p>
        <a href='#/' class='btn' ><span>${btn_description}</span></a>
    </div>
    `
    style.innerHTML+=`
    .papa-talk-container
    {
        display:flex;
        justify-content:center;
        max-width:1150px;
        width:100%;
        margin:5rem auto 0;
        gap:3rem;
        background-color:#CFEAE2;
        padding: 3rem 1rem; 
    }
    .papa-talk-container .aside-content
    {
        width:45%;
    }
    .papa-talk-container .aside-content h2
    {
        font-weigth:900;
    }
    .papa-talk-container .aside-content p 
    {
        margin:1.5rem 0;
        font-size:.9rem;
        color:#616161;
    }
    .papa-talk-container .aside-image
    {
        width:45%;
    }

    .papa-talk-container .aside-image img
    {
        width:100%;
        height:100%;
        object-fit:cover;
    }
    .btn
    {
        background-color:#008764;
        color:white;
        padding:.25rem .5rem;
        border-radius:6px;
        font-size:15px;
        display:flex;
        flex-flow:row wrap;
        width:fit-content;
    }
    @media (max-width:767px)
    {
        .papa-talk-container
        {
            flex-flow:column;
            justify-content-center;
            align-items:center;
        } 
        .papa-talk-container .aside-content,  .papa-talk-container  .aside-image
        {
            width:100%;
        }
    }
    `
    return contactFreeDelivery
}