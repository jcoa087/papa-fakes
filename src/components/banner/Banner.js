



export default function Banner()
{
    const appBanner = document.createElement('section'),
    container = document.createElement('div'),
    style = document.getElementById('dinamic-style')

    appBanner.classList.add('app-banner')
    container.classList.add('container')
    
    container.innerHTML=`
    <div class='app-banner-text' >
        <h1>Descarga nuestra aplicación</h1>
        <p>Recibe nuestras mejores ofertas directo en tu dispositivo y pide en Papa John's en donde estés.</p>
        <ul>
            <li><a><img src='src/assets/icons/banner/store.svg' alt='Apple store' /></a></li>
            <li><a><img src='src/assets/icons/banner/play.png' alt='Google play' /></a></li>
        </ul>
    </div>
    <img src='src/assets/icons/banner/pj-app-cta-mex.png' alt='download the app' />
    `
    appBanner.appendChild(container)

    style.innerHTML+=`
    .app-banner
    {
        padding:1.5rem;
        width:100%;
        max-width:1150px;
        margin:4rem auto 0;
    }
    .app-banner .container
    {

        width:100%;

        display:flex;
        alignt-items:center;
        justify-content:flex-end;
        gap:5rem;
    }
    .app-banner .container .app-banner-text
    {
        display:flex;
        flex-flow: column;
        justify-content:center;
        align-items:flex-end;
        width:45%;
        text-align: end;
    }
    .app-banner .container .app-banner-text h1
    {
        font-size:2rem;
        font-weight:900;
        text-shadow: 1px 0 #000;
        letter-spacing:1px
    }
    .app-banner .container .app-banner-text p
    {
        width:70%;
        color:#7B7B7B;
    }
    .app-banner .container .app-banner-text ul
    {
        display:flex;
        justify-content:center;
        align-items:center;
    }
    .app-banner .container .app-banner-text ul li  a img
    {
        width:clamp(50px,8vw + 5rem,190px);
        height:clamp(20px,4vw + 4rem,80px);
    }
    .app-banner .container img
    {
        width:35%;
    }
    @media(max-width:767px)
    {

        .app-banner .container .app-banner-text p
        {
            text-align:justify;
        }
        .app-banner .container .app-banner-text,.app-banner .container .app-banner-text p
        {
            width:100%;
            justify-content:center;
            align-items:center;
        }
      
        .app-banner .container > img
        {
            display:none;
        }
    }
    `
    return appBanner
}