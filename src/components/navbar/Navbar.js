import NavbarCss from "./NavbarCss.js"




const state={show:false}

const render=(component,state)=>{
    component.innerHTML=''
    component.innerHTML=template(state)
}


const handleMobile=()=>{
    document.addEventListener('click',(e)=>{
        if(!e.target.matches('.mobile-menu-btn'))return false
        e.preventDefault()
        const $navbar = document.querySelector('.navbar')
        state.show=true
        render($navbar,state.show)
    })
}
const handleClose=()=>{
    document.addEventListener('click',(e)=>{
        if(!e.target.matches('.mobile-menu-close'))return false
        e.preventDefault()
        const $navbar = document.querySelector('.navbar')
        state.show=false
        render($navbar,state.show)
    })
}

const template=(state)=>{
    console.log(`state: ${state}`)
    return `
    <div class='navbar-content'>
        <div class='navbar-top' >
            <a href='#/' ><img class='navbar-brand-icon' src='src/assets/icons/icon.png' alt='Papa fakes pizza' /></a>
            <div class='navbar-language' >
                <img src='src/assets/icons/globe-wire.svg' />
                <button>Español<img src='src/assets/icons/chevron-down.svg' /></button>
    
            </div>
        </div>
        <hr/>
        <div class='navbar-bottom'>
            <ul class='navbar-menu'>
                <li class='navbar-menu-link'><a href='#/menu' >Menú</a></li>
                <li class='navbar-menu-link' ><a href='#/promotions' >Promociones</a></li>
                <li class='navbar-menu-link' ><a href='#/papa-talk' >Papa Talk</a></li>
                <li class='navbar-menu-link' ><a href='#/papa-rewards' >Papa Rewards</a></li>
                <li class='navbar-menu-link' ><a href='#/about-us' >Acerca de nosotros</a></li>
            </ul>
            <a class='order-btn' href='#/order' >¡Ordena ya!</a>
        </div>
        <img  src='src/assets/icons/menu-icon.svg' class='mobile-menu-btn' />
    </div>

    <div class=${state?"'mobile-menu active'":'mobile-menu'}>
        <div class='mobile-btn-container' >
            <img  src='src/assets/icons/x-close.svg' class='mobile-menu-close' />
        </div>
        <ul class='navbar-mobile-menu'>
            <li class='navbar-mobile-menu-link'><a href='#/menu' >Menú</a></li>
            <li class='navbar-mobile-menu-link' ><a href='#/promotions' >Promociones</a></li>
            <li class='navbar-mobile-menu-link' ><a href='#/papa-talk' >Papa Talk</a></li>
            <li class='navbar-mobile-menu-link' ><a href='#/papa-rewards' >Papa Rewards</a></li>
            <li class='navbar-mobile-menu-link' ><a href='#/about-us' >Acerca de nosotros</a></li>
        </ul>
        <a class='mobile-order-btn' href='#/order' >¡Ordena ya!</a>
        <hr class='mobile-hr' />
        <div class='mobile-navbar-language' >
            <img src='src/assets/icons/globe-wire.svg' />
            <button>Español<img src='src/assets/icons/chevron-down.svg' /></button>
        </div>
    </div>
`
}
export default function Navbar(){

    const $navbar = document.createElement('header')


    $navbar.classList.add('navbar')
    $navbar.innerHTML=template(state.show)
    NavbarCss()
    handleMobile()
    handleClose()
    return $navbar
}

