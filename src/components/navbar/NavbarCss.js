


export default function NavbarCss()
{
    const $style = document.getElementById('dinamic-style')
    $style.innerHTML=`
    .navbar
    {
        background-color:#fff;
        width:100%;
        height:100px;
        box-shadow:1px 1px 1px 1px rgba(0,0,0,.1);
        position:sticky;
        top:0;
        z-index:2;
    }
    
    .navbar-content
    {
        margin:auto;
        max-width:1150px;
        width:100%;
        height:100%;
        // border:1px solid red;
    
        padding:.8rem;
    }
    
    .navbar-content .navbar-top
    {
    
        display:flex;
        justify-content:space-between;
        // border:1px solid red;
        width:100%;
        height:40%;
    }
    .navbar-content .navbar-top .navbar-language
    {
        display:flex;
        align-items:center;
        gap:1rem;    
    }
    
    .navbar-content .navbar-top .navbar-language button
    {
        border:none;
        border-radius:2px;
        font-weight:600;
        background-color:transparent;
        font-size:1rem;
        cursor:pointer;
    }
    .navbar-bottom
    {
        display:flex;
        align-items:flex-end;
        justify-content:space-between;
        // border:1px solid red;
        width:100%;
        height:60%; 
    }   
    .navbar-menu
    {
        display:flex;
        justify-content:space-between;
        gap:2rem;  
    }
    .navbar-brand-icon
    {
        width:130px;
        height:28px;
    }
    
    .order-btn
    {
        transition:.5s;
        color:white;
        background-color:#CE2735;
        padding: .25rem  .5rem 0;
        border-radius:4px;
        border-bottom:4px solid transparent;
    }
    .navbar-menu-link
    {
        position:relative;
    }
    .navbar-menu-link:hover::after
    {
        position:absolute;
        content:'';
        background-color:#376B53;
        display:block;
        width:100%;
        height:4px;
        bottom:-15px;
    }
    /*MOBILE*/
    .mobile-menu
    {
        display:none;
    }
    
    .mobile-btn-container
    {
        width:100%;
        height:30px;
        display:flex;
        justify-content:flex-end;
        padding-right:1rem;
    }
    
    
    .mobile-menu-btn
    {
        display:none;
        cursor:pointer;
    }
    .mobile-order-btn
    {
        font-size:.8rem;
        transition:.5s;
        color:white;
        background-color:#CE2735;
        padding: .25rem  .5rem 0;
        border-radius:4px;
        border-bottom:4px solid transparent;
    }
    .mobile-navbar-language button
    {
        border:none;
        background-color:transparent;
    }
    .mobile-menu-close
    {
        cursor:pointer;
    }
    @media(max-width:900px)
    {
        .navbar-menu .navbar-menu-link a
        {
            font-size:.9rem;
        }
        .order-btn
        {
            font-size:.8rem; 
        }
        
    }
    
    @media(max-width:767px)
    {
        .navbar
        {
            height:50px;
          
        }
        .navbar-content
        {
            display:flex;
            align-items:center;
            justify-content:space-between;
        }
        .navbar-content .navbar-top
        {
            // border:1px solid black;
            height:fit-content;
            width:190px;
        }
    
        .navbar-content  .navbar-bottom
        {
            // border:1px solid black;
            height:fit-content;
            width:190px;
        }
    
    
        .navbar-menu,.navbar-content .navbar-top .navbar-language
        {
            display:none;
        }
        hr
        {
            display:none;
        }
        .navbar-content
        {
    
            display:flex;
        }
        
        .mobile-hr
        {
            display:block;
            margin:1rem 0;
        }
        .mobile-menu-btn
        {
            display:block;
        }
        .mobile-menu.active
        {   
            padding:1rem 0 0 1.4rem;
            display:block;
            width:100%;
            height:100vh;
            position:absolute;
            background-color:#fff;
            top:0;
            left:0;
        }
    }
    
    `
}

{/* <ul>
<li>Español</li>
<li>Inglés</li>
</ul> */}