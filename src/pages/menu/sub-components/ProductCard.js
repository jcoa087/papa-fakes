



export default function ProductCard(img,name,price,description)
{
    return`
    <div class='product-card'>
        <img src=${img} alt='Paquete de promoción'/>
        <div class='menu-product'>
            <div class='menu-product-info'>
                <span class='menu-product-name' >${name}</span>
                <span class='menu-product-price'>${price}</span>
                <p class='menu-product-description'>${description}</p>
            </div>
        </div>
    </div>
    `
}