import ProductCard from "./sub-components/ProductCard.js"


const $style = document.getElementById('dinamic-style')

export default function Menu()
{
    const $menu = document.createElement('div')
    $menu.classList.add('menu-page')
    
    $menu.innerHTML=`
    <section class='menu-banner' >
        <img src='src/assets/images/menu/promotions/discount-banner.jpg' />
    </section>
    <section class='promotions' >

        <div class='container'>
            <h1>PROMOCIONES</h1>
            <div class='menu-promotions'>

            ${ProductCard('src/assets/images/menu/promotions/200662-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/promotions/189937-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/promotions/189935-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/promotions/200663-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/promotions/200662-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            
            </div>
        </div>
        <div class='container'>
            <h1>PIZZAS</h1>
            <div class='menu-promotions'>

            ${ProductCard('src/assets/images/menu/pizzas/189904-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas/191106-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
    
            </div>
        </div>
        <div class='container'>
            <h1>PIZZAS SIGNATURE</h1>
            <div class='menu-promotions'>

            ${ProductCard('src/assets/images/menu/pizzas-signature/189895-700.jpg','All The Meats','$354.00','Una obra maestra con carnes de alta calidad que incluye pepperoni, tocino, jamón, carne de res y salchicha de puerco. ¿Una nueva favorita?')}
            ${ProductCard('src/assets/images/menu/pizzas-signature/189905-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-signature/189881-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-signature/205790-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-signature/189873-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-signature/189855-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
    
            </div>
        </div>
        <div class='container'>
            <h1>PIZZAS ESPECIALIDAD</h1>
            <div class='menu-promotions'>

            ${ProductCard('src/assets/images/menu/pizzas-specialty/189861-700.jpg','All The Meats','$354.00','Una obra maestra con carnes de alta calidad que incluye pepperoni, tocino, jamón, carne de res y salchicha de puerco. ¿Una nueva favorita?')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/189900-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/189878-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/189870-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/189857-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/189884-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/189882-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/pizzas-specialty/200661-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
    
            </div>
        </div>

        <div class='container'>
            <h1>COMPLEMENTOS</h1>
            <div class='menu-promotions'>

            ${ProductCard('src/assets/images/menu/sides/189835-700.png','All The Meats','$354.00','Una obra maestra con carnes de alta calidad que incluye pepperoni, tocino, jamón, carne de res y salchicha de puerco. ¿Una nueva favorita?')}
            ${ProductCard('src/assets/images/menu/sides/189844-700.png','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189830-700.png','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189840-700.png','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/191132-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189845-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189822-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189838-700.png','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189837-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
            ${ProductCard('src/assets/images/menu/sides/189839-700.jpg','Pizza orilla de queso + 1 Complemento + 1 Refresco de 2L','$354.00','Elige tu pizza grande de masa orilla de queso + 1 Complemento + Refresco 2L de la Familia Coca Cola')}
    
            </div>
        </div>
        
    </section>
    `

    $style.innerHTML+=`
    .menu-page
    {
        max-width:1150px;width:100%;
        margin:.25rem auto 0;
    }
    .menu-banner
    {
        width:100%;
    }
    .menu-banner img
    {
        width:100%;
    }

    .menu-page .menu-promotions
    {
        width:75%;
        height:100%;
        display: grid;
        gap: .5rem;
        grid-template-columns: repeat(auto-fit,minmax(25%,1fr));
        grid-auto-rows:23rem;
        justify-content: center;
        padding:1.2rem;
    }

    .menu-page .menu-promotions .product-card
    {
        padding: 1rem;
        font-size:.8rem;
        transition:.5s;
        border-radius:15px;
    }
    .menu-page .menu-promotions .product-card:hover
    {
        box-shadow:1px 4px 10px 1px rgba(0,0,0,.2);
        transform: translateY(-3px);
        cursor:pointer;
    }
    .menu-page .menu-promotions .product-card img
    {
        width:100%
    }
    .menu-product-name
    {
        font-weight:900;
    }
    .menu-page .promotions h1
    {
        color:#000; text-shadow: 2px 0 #000; 
        letter-spacing:2px; font-weight:bold
    }
    @media (max-width:1010px)
    {
        .menu-page .menu-promotions
        {
            grid-template-columns: repeat(auto-fit,minmax(40%,1fr));
        }
    }
    @media (max-width:780px)
    {
        .menu-page .menu-promotions
        {
            grid-template-columns: repeat(auto-fit,minmax(50%,1fr));
        }
    }
    `
    return $menu
}

