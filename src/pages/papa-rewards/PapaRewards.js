import AsideContainer from "./AsideContainer.js"
import IconList from "./IconList.js"
import Banner from "../../components/banner/Banner.js"


export default function PapaRewards()
{
    const 
    papaRewards = document.createElement('section'),
    container = document.createElement('div'),
    style=document.getElementById('dinamic-style')

    papaRewards.classList.add('papa-rewards')
    container.classList.add('container')

    container.innerHTML=`
    <h1>Come pizza. Gana premios. Bienvenido.</h1>
    `
    container.appendChild(AsideContainer())
    container.appendChild(IconList())
    container.appendChild(Banner())
    style.innerHTML+=`
    .papa-rewards
    {
        width:100%;
        margin:5rem 0;
    }
    
    .papa-rewards .container
    {
        width:100%;
        margin: auto;
    }
    .papa-rewards .container h1
    {
        font-size:2.5rem;
        max-width:1150px;
        width:100%;
        margin:auto;
    }
    .papa-rewards .container .content
    {
        max-width:1150px;
        width:100%;
        margin:auto;
        display:grid;
        grid-template-columns:50% 50%;
        gap:1rem;
    }


    .papa-rewards .container .content .image-aside
    {
        width:100%;
        height:100%;
        display:flex;
        flex-flow:column;
        justify-content:center;     
        align-items:center;
    }
    .papa-rewards .container .content .image-aside img
    {
        width:90%;
        height:95%;
        object-fit:cover;
    }
    .papa-rewards .container .icon-list .icons
    {
        max-width:1150px;
        width:100%;
        margin:auto;
        display:grid;
        grid-template-columns:repeat(auto-fit,minmax(100px,1fr));
        gap:3rem;
    }
    .papa-rewards .container .icon-list .icons li
    {
        display:flex;
        align-items:center;
        flex-flow:column;
        gap:2rem;
    }

    .papa-rewards .container .icon-list .icons li img
    {
        width:60px;
    }
    .papa-rewards .container .icon-list .icons li p
    {
        text-align:center;
        color:#6F6F6F;
    }
    .papa-rewards .container .icon-list .icon-title
    {
        max-width:1150px;
        width:100%;
        margin:auto; 
        display:flex;
        justify-content:center;
    }

    @media (max-width:767px)
    {
        
        .papa-rewards .container .content
        {
            padding:1rem;
            grid-template-columns:repeat(auto-fit,minmax(100%,1fr));
        }
    }
    `
    papaRewards.appendChild(container)
    return papaRewards
}