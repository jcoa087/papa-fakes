




export default function IconList()
{
    const iconList = document.createElement('section'),
    container = document.createElement('div'),
    style=document.getElementById('dinamic-style')
    iconList.classList.add('icon-list')
    container.classList.add('container')

    // style="background-color: rgb(206, 234, 225);"
    container.innerHTML=`
    <div class='icon-title' ><h2>4 sencillos pasos</h2></div>
    <ul class='icons' >
        <li>
            <img src='src/assets/icons/icon-list/pj-icon-tick.png' alt='Black and white icon' />
            <h3>Registrarse</h3>
            <p>Para unirte a los Papa Rewards, crea una cuenta en la página web o en la aplicación.</p>
        </li>
        <li>
            <img src='src/assets/icons/icon-list/pj-icon-pay-online.png' alt='Black and white icon' />
            <h3>Ordenar pizza</h3>
            <p>Pide en línea, por teléfono, o cuando visites un punto de venta.</p>
        </li>
        <li>
            <img src='src/assets/icons/icon-list/pj-icon-receive-reward.png' alt='Black and white icon' />
            <h3>Gana puntos</h3>
            <p>A tu cuenta, se añade un punto por cada $100 MXN.</p>
        </li>
        <li>
            <img src='src/assets/icons/icon-list/pj-icon-pizza.png' alt='Black and white icon' />
            <h3>Recibe premios</h3>
            <p>¡Usa tus puntos para comprar más pizza deliciosa!</p>
        </li>
    </ul>
    `
    iconList.appendChild(container)
    style.innerHTML+=`
    .icon-list
    {   
        width:100%;
        background-color: rgb(206, 234, 225);
        min-height:459px;
        padding:6rem 0;
    }

    `
    return iconList
}