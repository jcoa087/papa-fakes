


export default function AsideContainer()
{
    const asideContainer = document.createElement('div'),
    style = document.getElementById('dinamic-style')
    asideContainer.classList.add('content')

    asideContainer.innerHTML=`
    <div class='aside-content'>
        <h2>Gana puntos cada vez que hagas un pedido con nuestro programa de lealtad</h2>
        <p>¡Date un gusto, y luego deja que te demos el gusto! </p>
        <p><i>Gasta $ 100 MXN y gana 1 punto. Acumule 10 puntos y le daremos un cupón de $ 125 MXN para usar en su próximo pedido.</i>
        Cuando creas una cuenta, quedas registrado, de manera automática, en nuestro programa de Papa Rewards.</p>
        <p>Aplican términos y condiciones.</p>
        <a href='#/' ><span>Haz tu primer pedido hoy para ganar puntos</span></a>
    </div>
    <div class='image-aside'>
        <img src='src/assets/images/papa-rewards/pj-promo-image-large-papa-rewards.webp' alt='Papa rewards' />
    </div>
    `

    style.innerHTML+=`
    .papa-rewards .container .content .aside-content
    {
        width:90%;
        height:100%;
        display:flex;
        flex-flow:column;

        justify-content:center;
    }
    .papa-rewards .container .content .aside-content a
    {
        background-color:#007B53;
        color:white;
        width:fit-content;
        padding:.5rem;
        border-radius:6px;
        font-size:16px;
    }
    
    .papa-rewards .container .content .aside-content p
    {
        margin:0; font-size:1.1rem;
        font-weight:0; color:#777777;
        line-height:24px;
    }
    `
    return asideContainer
}