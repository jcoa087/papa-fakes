
import Banner from '../../components/banner/Banner.js'


export default function Promotions()
{

    const promotions = document.createElement('section'),
    container = document.createElement('div'),
    style = document.getElementById('dinamic-style')
    
    promotions.classList.add('promotions')
    container.classList.add('container')

    container.innerHTML=`
    <h1>Encuentra las mejores ofertas especiales todos los días</h1>
    <div>
        <h2>Importante</h2>
        <p>Las promociones y los descuentos pueden variar según la ciudad, el estado y/o aplicarse solo en las tiendas participantes. Seleccione una tienda a continuación para ver todas sus promociones y descuentos locales.
        </p>
        <a href='#/' class='btn' ><span>Seleccione su tienda local</span></a>
    </div>
    `
    
    container.appendChild(Banner())
    promotions.appendChild(container)
    style.innerHTML+=`
    .container
    {
        max-width:1150px;
        margin:auto;
    }
    .container > div
    {
        margin:5rem 0;

    }
    `

    return promotions

}