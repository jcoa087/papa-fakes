
import PromoRow from "./sub-components/promo-row/PromoRow.js"
import ContactFreeDelivery from './sub-components/contact-free-delivery/ContactFreeDelivery.js'
import KnowMore from './sub-components/know-more/KnowMore.js'
import PapaTalkContainer from '../../components/papa-talk-container/PapaTalkContainer.js' 
import Banner from "../../components/banner/Banner.js"
const $style = document.getElementById('dinamic-style')

export default function Home()
{   

    const home = document.createElement('div')
    home.classList.add('home')
    home.appendChild(PromoRow())
    home.appendChild(ContactFreeDelivery())
    home.appendChild(KnowMore())
    home.appendChild(PapaTalkContainer('Apreciamos mucho tus observaciones, así que dinos lo que opinas','Cuando hagas tu pedido, busca nuestra encuesta de evaluación Papa Talk en tu correo electrónico. Ahí podrás decirnos cómo estuvo tu pedido. Tu opinión es muy valiosa, y tus observaciones nos ayudan a mejorar nuestro servicio. Y nuestra pizza.','¿No recibió su enlace Papa Talk? Haga clic a continuación y deje sus comentarios aquí.','Complete su encuesta de Papa Talk'))
    home.appendChild(Banner())
    return home
}