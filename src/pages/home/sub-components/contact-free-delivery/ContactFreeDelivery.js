



export default function ContactFreeDelivery()
{


    const contactFreeDelivery = document.createElement('section'),style = document.getElementById('dinamic-style')
    contactFreeDelivery.classList.add('contact-free-delivery')
    contactFreeDelivery.innerHTML=`
    <div class='aside-content' >
        <h2>Te entregamos la pizza sin contacto </h2>
        <p>Estamos cuidando de tu seguridad, incluso desde el momento en que hacer tu orden y hasta el momento en que la recibes. Tú pagas de manera anticipada en línea y nosotros dejamos tu pedido en la puerta y nos apartaremos para que puedas recibirlo.
        </p>
        <a href='#/' class='btn' ><span>Descubre cómo cuidamos de tu seguridad</span></a>
    </div>
    <div class='aside-image' >
        <img src='src/assets/images/contact-free-delivery/pj-promo-image-large-cfd-light-green-2.webp' />
    </div>
    `
    style.innerHTML+=`
    .contact-free-delivery
    {
        display:flex;
        justify-content:center;
        max-width:1150px;
        width:100%;
        margin:5rem auto 0;
        gap:3rem;
    }
    .aside-content
    {
        width:45%;
    }
    .aside-content h2
    {
        font-weigth:900;
    }
    .aside-content p 
    {
        margin:1.5rem 0;
        font-size:.9rem;
        color:#616161;
    }
    .aside-image
    {
        width:45%;
    }

    .aside-image img
    {
        width:100%;
        height:100%;
        object-fit:cover;
    }
    .btn
    {
        background-color:#008764;
        color:white;
        padding:.25rem .5rem;
        border-radius:6px;
        font-size:15px;
        display:flex;
        flex-flow:row wrap;
        width:fit-content;
    }
    @media (max-width:767px)
    {
        .contact-free-delivery
        {
            padding:1rem;
            flex-flow:column;
            justify-content-center;
            align-items:center;
        } 
        .aside-content,    .aside-image
        {
            width:100%;
        }
    }
    `
    return contactFreeDelivery
}