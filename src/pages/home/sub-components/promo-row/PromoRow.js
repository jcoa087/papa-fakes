

import { MenuCard } from "../../../../components/menu-card/MenuCard.js"
import PromoRowCss from "./PromoRowCss.js"
const navigate=(element)=>{
    element.addEventListener('click',(e)=>{
        if(!e.target.matches('.food-item'))return
        e.preventDefault()
        location.hash='#/menu'
    })
}

export default function PromoRow()
{

    const $style = document.getElementById('dinamic-style')

    $style.innerHTML+=PromoRowCss()
    const $promo_row = document.createElement('section')

    $promo_row.classList.add('promo-row')
    const $promo_row_content = document.createElement('div')

    $promo_row.innerHTML=`
    <h1>¿Tienes hambre? ¡Prueba nuestra pizza hoy!</h1>
    `

    $promo_row_content.classList.add('promo-row-content')
    $promo_row_content.appendChild(MenuCard('src/assets/images/home-promotions/pj-prod-img-pizza-hawaiian.webp','Hawaiana','¿Quién dijo que la fruta no puede estar en la pizza? Saborea el trópico con la dulce y jugosa piña y jamón.','Pide ahora'))
    $promo_row_content.appendChild(MenuCard('src/assets/images/home-promotions/pj-prod-img-pizza-super-pepperoni.webp','Super pepperoni','Nuestra Super Pepperoni tiene un súper sabor. Está cargada con pepperoni y queso mozzarella. Un pedacito de cielo hecho de pura carne.','Pide ahora'))
    $promo_row_content.appendChild(MenuCard('src/assets/images/home-promotions/pj-prod-img-pizza-the-works.webp','The works','Una súper pizza apilada para los súper amantes de la pizza. Tres carnes con verduras frescas y aceitunas: ¿qué más se puede pedir?','Pide ahora'))
    $promo_row_content.appendChild(MenuCard('src/assets/images/home-promotions/pj-menu-teaser-homepage-go-to-icon.webp','Ver el menú completo','Pizza, complementos, bebidas, extras y postres; ¡Lo tenemos todo!','Ver el menú completo'))
    
    $promo_row.appendChild($promo_row_content)
    navigate($promo_row)
    return $promo_row
}