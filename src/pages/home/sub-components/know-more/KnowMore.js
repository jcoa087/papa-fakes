

import { MenuCard } from "../../../../components/menu-card/MenuCard.js"
import PromoRowCss from "./KnowMoreCss.js"
const navigate=(element)=>{
    element.addEventListener('click',(e)=>{
        if(!e.target.matches('.food-item'))return
        e.preventDefault()
        location.hash='#/menu'
    })
}

export default function PromoRow()
{

    const $style = document.getElementById('dinamic-style')

    $style.innerHTML+=PromoRowCss()
    const $promo_row = document.createElement('section')

    $promo_row.classList.add('promo-row')
    const $promo_row_content = document.createElement('div')

    $promo_row.innerHTML=`
    <h1>¿Tienes hambre? ¡Prueba nuestra pizza hoy!</h1>
    `

    $promo_row_content.classList.add('promo-row-content')
    $promo_row_content.appendChild(MenuCard('src/assets/images/know-more/pj-promo-image-large-bw-slapping-dough.webp','Mejor pizza. Mejores ingredientes.','Es así de sencillo. Y siempre lo ha sido. ¡Incluso desde nuestra primera pizza!','Acerca de nosotros'))
    $promo_row_content.appendChild(MenuCard('src/assets/images/know-more/pj-promo-image-large-papa-rewards.webp','Come pizza. Gana premios.','Gana puntos cada que hagas un pedido con nuestro programa de fidelidad.','Papa Rewards'))
    $promo_row_content.appendChild(MenuCard('src/assets/images/know-more/pj-promo-image-large-mex-shop-front.webp','Encuentra tu restaurante local','¡Papa John’s está en Mexico! ¿Quieres probar una pizza americana?','Localizador de restaurante'))
    $promo_row_content.appendChild(MenuCard('src/assets/images/know-more/pj-promo-image-large-pizza-picnic.webp','No te pierdas de otras promociones','Regístrate para recibir ofertas a través de correo electrónico o mensajes de texto y así ahorrar dinero comprando tus productos favoritos.','Registrarse aqui'))
    
    $promo_row.appendChild($promo_row_content)
    navigate($promo_row)
    return $promo_row
}