


export default function PromoRowCss()
{
    return `
    .promo-row
    {
        max-width:1150px;
        width:100%;
        min-height:20vh;
        margin:6rem auto 0;

    }
    .promo-row h1
    {
        font-size:2.5rem;
        font-weight:900;
        margin-bottom:2rem;
    }
    .food-card
    {
        background-color:#fff;
        border-radius:6px;
        box-shadow:1px 1px 50px 10px #F1F1F1;
        transition:.5s;
        display:flex;
        align-items:center;
        flex-flow:column;
        justify-content:space-between;
    }
    .food-card img
    {
        width:100%;
        aspect-ratio:1/.72;
    }
    .item-content
    {
        display:flex;
        flex-flow:column;   
        justify-content:space-between;
        border:1px solid white;
        padding:1rem;
        width:100%;
        height:70%;
    }


    .item-content h2
    {
        font-size:  clamp(1rem,.9vw + .4rem,1.5rem);
    }
    .item-content a
    {
        display:flex;
        align-items:center;
        justify-content:flex-end;
        font-size:1rem;
    }
    .item-content a img
    {
        width:30px;
        font-weight:bold;
        color
    }
    .item-content p 
    {
        font-size:.9rem;
        color:#4F4F4F;
    }
    .food-card:hover
    {
        cursor:pointer;
        transform:translateY(-3px);
    }
    .promo-row-content
    {
        width:100%;
        height:100%;
        display: grid;
        gap: 2.5rem;
        grid-template-columns: repeat(auto-fit,minmax(200px,1fr));
        grid-auto-rows:30rem;
        justify-content: center;
    }

    .link-container
    {
        display:flex;
        align-items:center;
        justify-content:flex-end;
        border:1px solid red;

        font-swize:1rem;
        width:100%;
        height:fit-content;
    }
    .link-container a
    {
        font-size:1rem;
    }
    @media (max-width:767px)
    {
        .item-content hr
        {
            display:block;
        }
        .promo-row-content
        {
            grid-template-columns: repeat(auto-fit,minmax(40%,1fr));
        } 
    }
    `
}