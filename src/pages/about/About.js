import PapaTalkContainer from "../../components/papa-talk-container/PapaTalkContainer.js"
import Banner from "../../components/banner/Banner.js"
export default function About()
{

    const 
    about = document.createElement('section'),
    container = document.createElement('div')

    about.classList.add('about')
    container.classList.add('container')
    
    
    container.appendChild(PapaTalkContainer('Apreciamos mucho tus observaciones, así que dinos lo que opinas','Cuando hagas tu pedido, busca nuestra encuesta de evaluación Papa Talk en tu correo electrónico. Ahí podrás decirnos cómo estuvo tu pedido. Tu opinión es muy valiosa, y tus observaciones nos ayudan a mejorar nuestro servicio. Y nuestra pizza.','¿No recibió su enlace Papa Talk? Haga clic a continuación y deje sus comentarios aquí.','Complete su encuesta de Papa Talk'))
    container.appendChild(Banner())

    about.appendChild(container)
    return about

}